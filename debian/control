Source: ruby-terrapin
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: prasannassp <prasanna@student.tce.edu>,
           gowtham <gowthama@student.tce.edu>,
           Utkarsh Gupta <utkarsh@debian.org>
Section: ruby
Priority: optional
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               pry,
               rake,
               ruby-bourne,
               ruby-climate-control (<< 1.0),
               ruby-rspec
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-terrapin
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-terrapin.git
Homepage: https://github.com/thoughtbot/terrapin
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-terrapin
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Suggests: ruby-posix-spawn
Description: run shell commands safely, even with user-supplied values
 Apart from the basic, normal stuff, you can use nterpolated arguments. It also
 prevents attempts at being bad.
 .
 You can also see what's getting run. The 'Command' part it logs is in green
 for visibility!
 .
 Terrapin will only shell-escape what is passed in as interpolations to the run
 method. It WILL NOT escape what is passed in to the second argument of new.
 Terrapin assumes that you will not be manually passing user-generated data to
 that argument and will be using it as a template for your command line's
 structure.
 .
 The performance can be increased by installing the posix-spawn gem.
